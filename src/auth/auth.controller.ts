import {
  Body,
  Controller,
  Get,
  Post,
  Req,
  Res,
  UseGuards,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { ApiCreatedResponse, ApiOkResponse, ApiOperation } from '@nestjs/swagger';
import { Response } from 'express';
import { LoginRequestDto, LoginResponseDto } from 'src/users/dtos';
import { Role } from 'src/users/roles/role.enum';
import { AuthService } from './auth.service';
import { JwtAuthGuard } from './guards/jwt-auth.guard';
import { LocalAuthGuard } from './guards/local-auth.guard';
import { RoleGuard } from './guards/roles.guard';

@Controller('/auth')
export class AuthController {
  constructor(
    private readonly authService: AuthService,
    private readonly jwtService: JwtService,
  ) {}

  @Post('/login')
  @ApiOperation({
    summary: 'Login',
  })
  @ApiCreatedResponse({
    type: LoginResponseDto,
    description: 'Login user successfully',
  })
  @UseGuards(LocalAuthGuard)
  async login(
    @Req() req,
    @Body() body: LoginRequestDto,
  ): Promise<LoginResponseDto> {
    const result = await this.authService.login(req.user);
    return result;
  }

  @ApiOperation({
    summary: 'Test authentication',
  })
  @ApiOkResponse({
    description: 'Test authentication when login',
  })
  @UseGuards(JwtAuthGuard)
  @Get('admin')
  async getAdmin() {
    //const user = this.jwtService.decode(req.cookies.Authentication);
    return 'authentication'
  }

  @ApiOperation({
    summary: 'Test authorization',
  })
  @ApiCreatedResponse({
    description: 'Test authorization after login',
  })
  @UseGuards(RoleGuard(Role.Buyer))
  @UseGuards(JwtAuthGuard)
  @Get('admin2')
  async getAdmin2(@Req() req, @Res() res) {
    const user = this.jwtService.decode(req.cookies.Authentication);
    res.status(200).send(user);
  }
}
