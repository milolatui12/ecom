import { Injectable, UnauthorizedException } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { Strategy } from 'passport-local';
import { LoginRequestDto, LoginResponseDto } from 'src/users/dtos';
import { Users } from 'src/users/users.schema';
import { AuthService } from '../auth.service';

@Injectable()
export class LocalStrategy extends PassportStrategy(Strategy) {
  constructor(private readonly authService: AuthService) {
    super({
      usernameField: 'email'
    });
  }

  async validate(email: string, password: string): Promise<Users> {
    console.log(email, password)
    const body = new LoginRequestDto(email, password)
    const user = await this.authService.authenticateUser(body);
    if (!user) {
      throw new UnauthorizedException();
    }
    return user;
  }
}
 