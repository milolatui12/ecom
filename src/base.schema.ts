import { Prop, Schema as SchemaNestJs } from "@nestjs/mongoose";
import { Document, ObjectId } from "mongoose";

@SchemaNestJs()
export class BaseSchemaNestjs extends Document {
    _id: ObjectId

	@Prop({
		type: Date,
		default: Date.now,
	})
	createdAt: Date

	@Prop({
		type: Date,
		default: Date.now,
	})
	updatedAt: Date
}