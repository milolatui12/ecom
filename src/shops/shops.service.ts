import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { CreateShopRequestDto } from './shop.dto';
import { Shops, ShopsDocument } from './shops.schema';

@Injectable()
export class ShopsService {
  constructor(
    @InjectModel(Shops.name) private shopModel: Model<ShopsDocument>,
  ) {}

  // async create(shop: CreateShopRequestDto, userId: string): Promise {
  //   return this.shopModel.create({
  //     shop,
  //     userId
  //   })
  // }

  async newProd(shopId, prodId) {
    return await this.shopModel.updateOne(
      { _id: shopId },
      { $push: { prodIds: prodId } },
    );
  }
}
