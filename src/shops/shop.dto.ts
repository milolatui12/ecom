import { ApiProperty } from "@nestjs/swagger";
import { IsNotEmpty, IsString } from "class-validator";
import { Shops } from "./shops.schema";

export class CreateShopRequestDto {
  @IsNotEmpty()
  @ApiProperty()
  @IsString()
  name: string;

//   @IsNotEmpty()
//   @ApiProperty()
//   @IsString()
//   userId: string;
}

export class ShopResponeDto {
    @ApiProperty()
    name: string

    @ApiProperty()
    userId: string
    
    constructor(shop: Shops) {
      this.name = shop.name || null
      this.userId = shop.userId || null
    }
}