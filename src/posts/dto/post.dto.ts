import { ApiProperty } from "@nestjs/swagger";
import { IsNotEmpty, IsString } from "class-validator";
import { ObjectId } from "mongoose";
import { Post } from "../post.schema";

export class CreatePostRequestDto {
    @IsNotEmpty()
    @IsString()
    @ApiProperty()
    content: string

    @IsNotEmpty()
    @IsString()
    @ApiProperty()
    userId: ObjectId
}

export class PostResponseDto {
    @ApiProperty()
    _id: ObjectId

    @ApiProperty()
    content: string
    
    @ApiProperty()
    userId: ObjectId
    
    @ApiProperty()
    createdAt: Date

    @ApiProperty()
    updatedAt: Date

    constructor(post: Post) {
        this._id = post._id
        this.content = post.content
        this.createdAt = post.createdAt
        this.updatedAt = post.updatedAt
        this.userId = post.userId
    }
}