import { Body, Controller, Get, Post } from '@nestjs/common';
import { ApiOkResponse, ApiOperation } from '@nestjs/swagger';
import { ReqUserProfile } from 'src/common/req-profile.decorator';
import { IUser } from 'src/users/interfaces/user.interface';
import { CreatePostRequestDto, PostResponseDto } from '../dto/post.dto';
import { PostService } from '../post.service';

@Controller('user/post')
export class PostController {
    constructor(
        private readonly postService: PostService
    ){}

    @Post()
    @ApiOperation({
		summary: 'Create new post 🌟',
	})
	@ApiOkResponse({
		type: PostResponseDto,
		description: 'Create new post successfully 🎉',
	})
    async createPost(
        // @ReqUserProfile() user: IUser,
        @Body() body: CreatePostRequestDto
    ): Promise<PostResponseDto> {
        return this.postService.createPost(body)
    }
}