import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import mongoose, { Document, Mongoose, ObjectId } from "mongoose";
import { BaseSchemaNestjs } from "src/base.schema";

export type PostDocument = Post & Document

@Schema()
export class Post extends BaseSchemaNestjs {
    @Prop()
    content: string

    @Prop({ type: mongoose.Schema.Types.ObjectId})
    userId: ObjectId
}

export const PostSchema = SchemaFactory.createForClass(Post)
