import { BullModule } from '@nestjs/bull';
import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { PostController } from '../controllers';
import { PostConsummer } from '../post.processor';
import { Post, PostSchema } from '../post.schema';
import { PostService } from '../post.service';

@Module({
  imports: [
    MongooseModule.forFeature([
      {
        name: Post.name,
        schema: PostSchema,
      },
    ]),
    BullModule.registerQueue({
        name: 'post'
    })
  ],
  controllers: [PostController],
  providers: [PostService, PostConsummer],
})
export class PostModule {}
