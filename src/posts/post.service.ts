import { InjectQueue } from '@nestjs/bull';
import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Queue } from 'bull';
import { Model } from 'mongoose';
import { IUser } from 'src/users/interfaces/user.interface';
import { CreatePostRequestDto, PostResponseDto } from './dto/post.dto';
import { Post, PostDocument } from './post.schema';

@Injectable()
export class PostService {
  constructor(
    @InjectModel(Post.name) private postModel: Model<PostDocument>,
    @InjectQueue('post') private postQueue: Queue,
  ) {}

  async createPost(
    // user: IUser,
    body: CreatePostRequestDto,
  ): Promise<PostResponseDto> {
    const { content, userId } = body;
    const postInfo = {
      content: content,
      userId: userId,
    } as Post;

    const post = await this.postModel.create(postInfo);

    await this.postQueue.add('noficate', {
        message: `User ${userId} has a new post!!`,
        content
    });

    return new PostResponseDto(post);
  }
}
