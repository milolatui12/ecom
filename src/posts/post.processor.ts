import { Processor, Process } from "@nestjs/bull";
import { Logger } from "@nestjs/common";
import { Job } from "bull";

@Processor('post')
export class PostConsummer {
    private readonly logger = new Logger(PostConsummer.name)

    @Process('noficate')
    async notificateToOrtherUser(job: Job<unknown>) {
        for (let i = 0; i < 20; i++) {
            this.logger.debug(job.data)
        }
    }
}