import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { BullModule } from '@nestjs/bull';

import { UsersModule } from './users/users.module';
import { ShopsModule } from './shops/shops.module';
import { ProdsModule } from './prods/prods.module';
import { CartsModule } from './carts/carts.module';
import { AuthModule } from './auth/auth.module';
import { PostModule } from './posts/modules';

import { AppController } from './app.controller';

import { AppService } from './app.service';

const url = process.env.MONGO_HOST || 'localhost';
const atlasUrl =
  'mongodb+srv://tritrannguyen255:milomilo1@cluster0.nwhd5.mongodb.net/nest';

@Module({
  imports: [
    // MongooseModule.forRoot(`${atlasUrl}`),
    BullModule.forRoot({
      redis: {
        host: 'localhost',
        port: 6379,
      },
    }),
    MongooseModule.forRoot(`mongodb://${url}:27017/ecom`),
    
    UsersModule,
    ShopsModule,
    ProdsModule,
    CartsModule,
    AuthModule,
    PostModule
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
